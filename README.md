--------------------------------------------------------------------
# 3D Velodyne (VLP16) to RGB Camera calibration
## For everybody who wants to find the external transform to match a pointcloud with an image.
--------------------------------------------------------------------
### Author: 
* Bjorn Lutjens (bjoern.luetjens@gmail.com)

### Source:
* ROS package 'lidar camera calibration' by Ankit Dhall
* https://github.com/ankitdhall/lidar_camera_calibration
* Paper: LiDAR-Camera Calibration using 3D-3D Point correspondences (https://arxiv.org/abs/1705.09785)

### Author affiliation: 
* Prof. Jonathan P. How, 
* Aerospace Control Laboratory, 
* Massachusetts Institute of Technology 
--------------------------------------------------------------------

--------------------------------------------------------------------
## How it works
--------------------------------------------------------------------

**Note:** Skip this if you are interested to get it running asap. 

**Goal:** The package outputs the transformation matrix of the camera (child frame) wrt. the velodyne (parent frame). 

**Idea:** Calculate the best transform to reduce the RMSE between each marker's 4 corner points, detected from the image and velodyne frame. 

**Input:** One camera RGB image, Velodyne VLP16 pointcloud, knowledge about marker. 

### Extraction of corner points from image:
A camera marker is placed in front of the camera. The included aruco package extracts the marker's midpoint and orientation. By knowing the marker's dimensions (config file), the algorithm computes the corner points for the image.  

### Extraction of corner points from velodyne:
The package prefilters the velodyne pointcloud FOV, based on the config file's input. Additionally, the package tries to filter out points on a plane. Ideally, that leaves behind all velodyne points on the markers' edge. Now, the user guesses which remaining points, belong the markers' edges. Afterwards, the algorithm fits a linear regression line on each edge and finds the points of intersection. The intersection points are taken as corner points.

### Calculation
Calculate the best RMSE transform, given each corner point transforms from lidar L_i and camera C_i

Rotation 
<img src="https://latex.codecogs.com/svg.latex?\Large&space;R = argmin_{R\in{SO(3)}}\lVert R(L_i - L)-(C_i - C)\rVert ^2" />

Translation 
<img src="https://latex.codecogs.com/svg.latex?\Large&space;T = \frac{1}{n} \sum^n_{i=1}{C_i} - R\frac{1}{n}\sum^n_{i=1}{L_i}" />

--------------------------------------------------------------------
## Installation
--------------------------------------------------------------------

#### Prerequisites
* Ubuntu 16.04
* ROS Kinetic Kame
* ROS camera driver
* ROS velodyne driver

#### Clone the ACL repository
```
cd /home/$USER/
git clone https://github.mit.edu/lutjens/lid_cam_cal.git
```

#### Download and install calibration package:
```
cd /home/$USER/lid_cam_cal/src/
git clone --depth=1 https://github.com/ankitdhall/lidar_camera_calibration.git lidar_camera_calibration
cd lidar_camera_calibration
sudo rm -r .git
cd ../..
mv src/lidar_camera_calibration/dependencies/aruco_mapping/ src/
mv src/lidar_camera_calibration/dependencies/aruco_ros/ src/
catkin_make
echo 'source /home/$USER/lid_cam_cal/devel/setup.bash' >> ~/.bashrc
```

--------------------------------------------------------------------
### Edit configuration files
--------------------------------------------------------------------

cd /home/$USER/lid_cam_cal/acl_config_files/

All ACL configuration files are extensively commented in \*\_commented.txt and (https://github.com/ankitdhall/lidar_camera_calibration)

#### I) Edit marker coordinates (marker_coordinates.txt)
  -  No changes necessary, if
    - You are using the ACL markers 

#### II) Edit the marker's location and camera intrinsics configuration file (config_file.txt)
  - No changes necessary, if 
    - 1) Your camera image's resolution is 640x480 and
    - 2) The marker's are placed in a 6x6x2.3m (width, height, depth) rectangle in front of the camera and
    - 3) Velodyne and camera both point towards the marker (Here, velodyne's front is opposite of its' cable) and
    - 4) The camera publishes its' intrinsic in a /camera_info topic.
  - If changes are desired, open config_file_commented.txt

#### III) Edit the camera image, camera info, and velodyne pointcloud ROS topic names (lidar_camera_calibration.yaml)
  - Substitute "/camera/color/image_raw" with your camera image topic
  - Substitute "/camera/color/camera_info" with your camera raw topic
  - Substitute "/velodyne_points" with your velodyne pointcloud topic
  - Note: Use $rostopic list to display topic names

#### IV) Edit the launch file (/launch/find_transform_acl.launch)
  - Substitute "/camera/color/image_raw" with your camera topic

#### V) Edit intrinsic camera calibration file for aruco_mapping pkg (acl_cam_conf.ini)
  - Display your camera info topic  
rostopic echo /camera/camera_info  
  - Copy K into camera matrix
  - Copy D into distortion matrix
  - Copy P into projection matrix
  - Note: Take care to delete all commas and additional spaces. If not, aruco_mapping can't parse the file.

#### Copy (with overwrite) all ACL configuration files into the workspace
```
cp /home/$USER/lid_cam_cal/acl_config_files/{marker_coordinates.txt,config_file.txt,lidar_camera_calibration.yaml} /home/$USER/lid_cam_cal/src/lidar_camera_calibration/conf/
cp /home/$USER/lid_cam_cal/acl_config_files/acl_cam_conf.ini /home/$USER/lid_cam_cal/src/aruco_mapping/data/
cp /home/$USER/lid_cam_cal/launch/find_transform_acl.launch /home/$USER/lid_cam_cal/src/lidar_camera_calibration/launch/
```

#### VI) Edit source file in order to output transformation of camera wrt. velodyne (instead of the velodyne wrt. camera): (find_velodyne_points.cpp)
  - Either) Copy full files:    
```
cp /home/$USER/lid_cam_cal/acl_config_files/find_velodyne_points.cpp /home/$USER/lid_cam_cal/src/lidar_camera_calibration/src/find_velodyne_points.cpp
cp /home/$USER/lid_cam_cal/acl_config_files/Find_RT.h /home/$USER/lid_cam_cal/src/lidar_camera_calibration/include/Find_RT.h
cp /home/$USER/lid_cam_cal/acl_config_files/marker.h /home/$USER/lid_cam_cal/src/aruco_ros/aruco/include/aruco/marker.h
```
  - Or) Copy line by line:
    - find_velodyne_points:
      - Copy line 18 (#include <tf/transform_broadcaster.h>) into /home/$USER/lid_cam_cal/src/lidar_camera_calibration/src/find_velodyne_points.cpp
      - Copy line 157-215 ("Code edited and created by Bjorn Lutjens, ACL, MIT")
    - Find_RT:
      - Change the function class of find_transformation() from void into Matrix4d
    - marker:
      - Outcomment the prints in friend ostream & operator() in line 123 to 132 


--------------------------------------------------------------------
### Physical Setup
--------------------------------------------------------------------

See pictures of the marker /home/$USER/lid_cam_cal/before_cal.png

Important remarks:
  - The markers represent IDs (written on the back). The one with lower ID has to be placed on the left, higher ID on the right.
  - Nothing should be behind the marker for at least a meter (s.t. obstacles behind are ignored by velodyne) 
  - The actual aruco marker has to be in the left middle corner of the cardboard
  - Make sure the camera's and velodyne's field of view see full marker

--------------------------------------------------------------------
### Calibration process
--------------------------------------------------------------------

roscore  
Launch Velodyne  
roslaunch velodyne_pointcloud VLP16_points_left.launch  
Launch Camera  
roslaunch realsense_camera r200_nodelet_modify_params.launch  
or  
roslaunch realsense_ros_camera camera.launch 

Start logging everything  
rosbag record /camera/color/camera_info /camera/color/image_raw /tf /tf_static /velodyne_left/velodyne_points /velodyne_right/velodyne_points /rosout /camera/depth/points

roslaunch  lidar_camera_calibration find_transform_acl.launch

- 1) "Mono8" window displays the mid-point and orientation of the marker (/docs/aruco_output.png)
- 2) "cloud" window displays all voxels that fall on the velodyne marker edge. See Troubleshooting if output differs from documentation (/docs/vel_output.png)

Input a 4-point polygon bounding box around every edge. Start with the top left edge of the left marker. Every corner-point is input by a click with the mouse on a pixel in the "cloud" window and a consecutive press of a key on the keyboard. Be sure to encircle "just enough" points on each edge, s.t. the algorithm can find the edge by linear regression. For documentation see  /docs/calibration_process.mp4 or https://youtu.be/SiPGPwNKE-Q.

The "polygon" window displays your input (/docs/edge_guess.png)  
The algorithm runs after all 4xn_markers polygons have been input.  
Wait until the 'polygon' and 'cloud' window disappears. You could see the iterations of matching points in the polygon window  
The results are printed in the console.  

Copy xyz translation and ypr rotation from command line as a tf static transform publisher into a ROS launch file:  
`<node pkg="tf" type="static_transform_publisher" name="vel_to_cam" args="tx ty tz y p r velodyne_frame camera_frame 1">  </node>`

Your done! Congrats  

For following velodyne-camera pairs, repeat steps III) to V) and "Calibration Process".  

--------------------------------------------------------------------
### Output interpretation:
--------------------------------------------------------------------

The program returns the transformation matrix of the camera wrt. the lidar frame. This means that the velodyne is the parent/root frame. (The original package outputs the transformation of the velodyne wrt. the camera. If the transformation of the velodyne wrt. the camera is desired, use the original find_velodyne_points.cpp)  
The camera coordinate system is equal to the ROS standard: (x to right parallel to ground, y into the ground, z in front)  

The relation of final and avg rotation is: final_rotation = rotation_avg * R_init_lidarToCamera  
R_init_lidarToCamera = rotation in last line of config_file.txt  
ROS convention of rotation order is: Z - Y - X   

--------------------------------------------------------------------
### Troubleshooting:
--------------------------------------------------------------------

#### Result inaccurate:
- Assure that nothing is behind the markers or adjust the marker bounding box (config_file.txt)
- Input "just enough" in the velodyne edge selection process, s.t. the algorithm can fit a curve by linear regression to find the corner positions.#
- Do not mark region of marker mount (pole)
- Try not to mark edges directly, as these could de associated to different edge
- The cross section of the marker that the velodyne sees is different than the cross section of the camera -- place marker further away to reduce error
- Velodyne sensor noise try various approaches to reduce velodyne sensor noise
- Enhance intrinsic camera calibration (http://wiki.ros.org/camera_calibration)
- Enhance intrinsic velodyne calibration 
- Use a camera without distortion (lidar-camera-calibration does not take distortion into account)
- Increase the number of markers

#### Aruco marker is not detected or not detected well:
- Assure that Aruco mapping identifies the markers
  - $ roslaunch aruco_mapping aruco_mapping.launch 
- Wiggle the marker a bit and observe if that increases the detection
- Assure good light conditions for aruco marker detection
- Enhance aruco marker detection with an image filter. Alleviates problems from strong reflections, poor lighting, bad focus. (https://github.com/SmartRoboticSystems/aruco_mapping_filter)
- Increase camera output resolution for aruco identification (max: 1920x1080)
  - Personal Test seemed to show that aruco is too slow to process 1920x1080 images
- Assure good black-white contrast with print quality 

#### None or too many Velodyne points in "cloud" window:
- Adjust last field of /conf/config_file.txt to set FOV rotation of velodyne for selection window
- Adjust the FOX bounding box parameters in config_file.txt
- Assure that depth in config_file.txt is set to ignore points behind marker.
- Assure markers are placed with great distance to objects beneath, such that algorithm does not take lidar points of obstacle behind and lidar accuracy is increased

#### If Fatal Error: Assertion failed:   
- Cause: Maybe only one point has been found on a velodyne edge. Consequently, linear regression to find the marker corner is not possible. 
- Assure that your mouse-input bounding boxes are big enough, that the algorithm finds at least two points per edge.
- Workaround: Check RMSE of last successful iteration and copy the printed values

