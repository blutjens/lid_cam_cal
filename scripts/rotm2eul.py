#####################################
# Converts a 3x3 Rotation Matrix into Euler RPY and vice versa.
#
# Author: Bjorn Lutjens (bjoern.luetjens@mit.edu)
# Source: https://www.learnopencv.com/rotation-matrix-to-euler-angles/
## Author affiliation: 
# Prof. Jonathan P. How, 
# Aerospace Control Laboratory, 
# Massachusetts Institute of Technology 
#####################################

import numpy as np
import math 

# Calculates Rotation Matrix given euler angles.
def eulerAnglesToRotationMatrix(theta) :
     
    R_x = np.array([[1,         0,                  0                   ],
                    [0,         math.cos(theta[0]), -math.sin(theta[0]) ],
                    [0,         math.sin(theta[0]), math.cos(theta[0])  ]
                    ])
         
         
                     
    R_y = np.array([[math.cos(theta[1]),    0,      math.sin(theta[1])  ],
                    [0,                     1,      0                   ],
                    [-math.sin(theta[1]),   0,      math.cos(theta[1])  ]
                    ])
                 
    R_z = np.array([[math.cos(theta[2]),    -math.sin(theta[2]),    0],
                    [math.sin(theta[2]),    math.cos(theta[2]),     0],
                    [0,                     0,                      1]
                    ])
                     
                     
    R = np.dot(R_z, np.dot( R_y, R_x ))
 
    return R


# Checks if a matrix is a valid rotation matrix.
def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6
 
 
# Calculates rotation matrix to euler angles
# The result is the same as MATLAB except the order
# of the euler angles ( x and z are swapped ).
def rotationMatrixToEulerAngles(R) :
 
    #assert(isRotationMatrix(R))
     
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
     
    singular = sy < 1e-6
 
    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])

# TODO
# 1. Use the ypr values from rot_avg to publish a static tf publisher from cam to vel. 

if __name__ == '__main__':
    rot_avg = np.array([[0.999281, 0.0106527, 0.0363972], [-0.00774042, 0.996826, -0.0792371], [-0.0371258, 0.0788984, 0.996191]])
    rot_final = np.array([[-0.0156095, 0.99983, -0.00985654], [0.079059, -0.00859272, -0.996833], [-0.996748, -0.0163393, -0.0789114]])
    
    R = rot_final
    
    #R = np.linalg.inv(R)
    print("Rotation matrix: ", R)
    print("Is rot matrix?: ", isRotationMatrix(R))
    rpy = rotationMatrixToEulerAngles(R)
    print("Euler Angles: (x, y, z) (roll, pitch,yaw)", rpy)

    r_orig = eulerAnglesToRotationMatrix(rpy)
    print("Original rotation: ", r_orig)
