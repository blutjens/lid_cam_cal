#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <algorithm>
#include <map>

#include "opencv2/opencv.hpp"

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <camera_info_manager/camera_info_manager.h>
// To publish final transform:
#include <tf/transform_broadcaster.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <pcl_ros/point_cloud.h>
#include <boost/foreach.hpp>
#include <pcl_conversions/pcl_conversions.h>
#include <velodyne_pointcloud/point_types.h>
#include <pcl/common/eigen.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/passthrough.h>

#include "lidar_camera_calibration/Corners.h"
#include "lidar_camera_calibration/PreprocessUtils.h"
#include "lidar_camera_calibration/Find_RT.h"

#include "lidar_camera_calibration/marker_6dof.h"

using namespace cv;
using namespace std;
using namespace ros;
using namespace message_filters;
using namespace pcl;


string CAMERA_INFO_TOPIC;
string VELODYNE_TOPIC;


Mat projection_matrix;

pcl::PointCloud<myPointXYZRID> point_cloud;

Eigen::Quaterniond qlidarToCamera; 
Eigen::Matrix3d lidarToCamera;

// Introduce iterator to know about final transformation
// TODO: do this more intelligent
int counter = 0;

void callback_noCam(const sensor_msgs::PointCloud2ConstPtr& msg_pc,
					const lidar_camera_calibration::marker_6dof::ConstPtr& msg_rt)
{
	ROS_INFO_STREAM("Velodyne scan received at " << msg_pc->header.stamp.toSec());
	ROS_INFO_STREAM("marker_6dof received at " << msg_rt->header.stamp.toSec());

	// Loading Velodyne point cloud_sub
	fromROSMsg(*msg_pc, point_cloud);

	point_cloud = transform(point_cloud, 0, 0, 0, config.initialRot[0], config.initialRot[1], config.initialRot[2]);

	//Rotation matrix to transform lidar point cloud to camera's frame

	qlidarToCamera = Eigen::AngleAxisd(config.initialRot[2], Eigen::Vector3d::UnitZ())
		*Eigen::AngleAxisd(config.initialRot[1], Eigen::Vector3d::UnitY())
		*Eigen::AngleAxisd(config.initialRot[0], Eigen::Vector3d::UnitX());

	lidarToCamera = qlidarToCamera.matrix();

	std:: cout << "\n\nInitial Rot" << lidarToCamera << "\n";
	point_cloud = intensityByRangeDiff(point_cloud, config);
	// x := x, y := -z, z := y

	//pcl::io::savePCDFileASCII ("/home/vishnu/PCDs/msg_point_cloud.pcd", pc);  


	cv::Mat temp_mat(config.s, CV_8UC3);
	pcl::PointCloud<pcl::PointXYZ> retval = *(toPointsXYZ(point_cloud));

	std::vector<float> marker_info;

	for(std::vector<float>::const_iterator it = msg_rt->dof.data.begin(); it != msg_rt->dof.data.end(); ++it)
	{
		marker_info.push_back(*it);
		std::cout << *it << " ";
	}
	std::cout << "\n";

	getCorners(temp_mat, retval, config.P, config.num_of_markers, config.MAX_ITERS);
	find_transformation(marker_info, config.num_of_markers, config.MAX_ITERS, lidarToCamera);
	
	//ros::shutdown();
}

void callback(const sensor_msgs::CameraInfoConstPtr& msg_info,
			  const sensor_msgs::PointCloud2ConstPtr& msg_pc,
			  const lidar_camera_calibration::marker_6dof::ConstPtr& msg_rt)
{

	ROS_INFO_STREAM("Camera info received at " << msg_info->header.stamp.toSec());
	ROS_INFO_STREAM("Velodyne scan received at " << msg_pc->header.stamp.toSec());
	ROS_INFO_STREAM("marker_6dof received at " << msg_rt->header.stamp.toSec());

	float p[12];
	float *pp = p;
	for (boost::array<double, 12ul>::const_iterator i = msg_info->P.begin(); i != msg_info->P.end(); i++)
	{
	*pp = (float)(*i);
	pp++;
	}
	cv::Mat(3, 4, CV_32FC1, &p).copyTo(projection_matrix);



	// Loading Velodyne point cloud_sub
	fromROSMsg(*msg_pc, point_cloud);

	point_cloud = transform(point_cloud, 0, 0, 0, config.initialRot[0], config.initialRot[1], config.initialRot[2]);

	//Rotation matrix to transform lidar point cloud to camera's frame

	qlidarToCamera = Eigen::AngleAxisd(config.initialRot[2], Eigen::Vector3d::UnitZ())
		*Eigen::AngleAxisd(config.initialRot[1], Eigen::Vector3d::UnitY())
		*Eigen::AngleAxisd(config.initialRot[0], Eigen::Vector3d::UnitX());

	lidarToCamera = qlidarToCamera.matrix();

	point_cloud = intensityByRangeDiff(point_cloud, config);
	// x := x, y := -z, z := y

	//pcl::io::savePCDFileASCII ("/home/vishnu/PCDs/msg_point_cloud.pcd", pc);  


	cv::Mat temp_mat(config.s, CV_8UC3);
	pcl::PointCloud<pcl::PointXYZ> retval = *(toPointsXYZ(point_cloud));

	std::vector<float> marker_info;

	for(std::vector<float>::const_iterator it = msg_rt->dof.data.begin(); it != msg_rt->dof.data.end(); ++it)
	{
		marker_info.push_back(*it);
		std::cout << *it << " ";
	}
	std::cout << "\n";

	getCorners(temp_mat, retval, projection_matrix, config.num_of_markers, config.MAX_ITERS);

	//****************************************************
	// Code edited and created by Bjorn Lutjens, ACL, MIT
	// Inverts final transformation matrix to transform camera wrt. velodyne
	// Publishes the final transform to ROS tf
	//****************************************************
	Matrix4d T_final(4,4);
	T_final.setIdentity(4,4);
	T_final = find_transformation(marker_info, config.num_of_markers, config.MAX_ITERS, lidarToCamera);

	counter += 1;

	if (counter >= config.MAX_ITERS){
 		std::cout << "***************************************\n";
 		std::cout << "Output for ACL\n";
 		std::cout << "***************************************\n";
		//std::cout << "Full final transformation of lidar wrt. camera is: \n" << T_final << "\n";

		static tf::TransformBroadcaster tf_broadcaster;
		tf::Transform T_final_tf;
		// Convert translation
		//std::cout << "x transl: " << T_final(0,3) << "\n";
		//std::cout << "y transl: " << T_final(1,3) << "\n";
		//std::cout << "z transl: " << T_final(2,3) << "\n";
		T_final_tf.setOrigin( tf::Vector3(T_final(0,3), T_final(1,3), T_final(2,3)) );
		// Convert rotation
		tf::Quaternion q;
		Matrix3d R_final(3,3);
		R_final << T_final(0,0), T_final(0,1), T_final(0,2),
			T_final(1,0), T_final(1,1), T_final(1,2), 
			T_final(2,0), T_final(2,1), T_final(2,2); 
		//std::cout << "Final rotation of lidar wrt. camera is: \n" << R_final << "\n";
		Eigen::Vector3d ypr_final = R_final.eulerAngles(2, 1, 0);
		//std::cout << "Final rpy of lidar wrt. camera is: \n" << ypr_final << "\n";
		q.setRPY(ypr_final(2), ypr_final(1), ypr_final(0));
		T_final_tf.setRotation(q);

		// Inverse lidar wrt. camera to get transform of camera wrt. lidar
		Matrix4d T_camToLid(4,4);
		T_camToLid = T_final.inverse();
		//std::cout << "Final transformation of camera wrt. lidar is: \n" << T_camToLid << "\n";
		Matrix3d R_camToLid(3,3);
		R_camToLid << T_camToLid(0,0), T_camToLid(0,1), T_camToLid(0,2),
			T_camToLid(1,0), T_camToLid(1,1), T_camToLid(1,2), 
			T_camToLid(2,0), T_camToLid(2,1), T_camToLid(2,2); 
		Eigen::Vector3d ypr_camToLid = R_camToLid.eulerAngles(2, 1, 0); // change orientation from camera coordinates to ROS coordinates
		std::cout << "Copy these values into ROS transform\n";
		std::cout << "Final xyz translation of camera wrt. lidar is: \n" << T_camToLid(0,3) << T_camToLid(1,3) << T_camToLid(2,3) << "\n";
		std::cout << "Final ypr of camera wrt. lidar is: \n" << ypr_camToLid << "\n";

		// Publish transform of lidar wrt. camera to tf
		while (ros::ok()){
			// TODO build a sleep.
			tf_broadcaster.sendTransform(tf::StampedTransform(T_final_tf, ros::Time::now(), "usb_cam_back_middle", "lidToCam"));
			std::cout << "send tf transform of lidar wrt. cam";
			ros::Duration(0.1).sleep(); // sleep for 0.1seconds
		}
	// TODO Invert returned matrix. Publish from velodyne to cam.
	}
}

// TODO this is a fake callback for testing, delete
void callbackTEST(const sensor_msgs::CameraInfoConstPtr& msg_info,
			  const sensor_msgs::PointCloud2ConstPtr& msg_pc,
			  const lidar_camera_calibration::marker_6dof::ConstPtr& msg_rt)
{

	// Test publish tf transform
	// Get final transformation and publish to ros tf
	Matrix4d T_final(4,4);
	T_final << 0.999281, 0.0106527, 0.0363972, -0.00837582, 
		-0.00774042, 0.996826, -0.0792371, -0.117147, 
		-0.0371258, 0.0788984, 0.996191, -0.0967684,
        0, 0, 0, 1;
	//T_lidToCam_final = find_transformation(marker_info, config.num_of_markers, config.MAX_ITERS, lidarToCamera);
	std::cout << "Final transformation is: \n" << T_final << "\n";

	static tf::TransformBroadcaster tf_broadcaster;
	tf::Transform T_final_tf;
	// Convert translation
	std::cout << "x transl: " << T_final(0,3) << "\n";
	std::cout << "y transl: " << T_final(1,3) << "\n";
	std::cout << "z transl: " << T_final(2,3) << "\n";
	T_final_tf.setOrigin( tf::Vector3(T_final(0,3), T_final(1,3), T_final(2,3)) );
	// Convert rotation
	tf::Quaternion q;
	Matrix3d R_final(3,3);
	R_final << T_final(0,0), T_final(0,1), T_final(0,2),
		T_final(1,0), T_final(1,1), T_final(1,2), 
		T_final(2,0), T_final(2,1), T_final(2,2); 
	std::cout << "Final rotation is: \n" << R_final << "\n";
	Eigen::Vector3d rpy_final = R_final.eulerAngles(0, 1, 2);
	std::cout << "Final rpy is: \n" << rpy_final << "\n";
	q.setRPY(rpy_final(0), rpy_final(1), rpy_final(2));
	T_final_tf.setRotation(q);
	// Publish to tf
	while (ros::ok()){
		tf_broadcaster.sendTransform(tf::StampedTransform(T_final_tf, ros::Time::now(), "usb_cam_back_middle", "lidToCam"));
	}
	ros::spin();
}


int main(int argc, char** argv)
{
	readConfig();
	ros::init(argc, argv, "find_transform");

	ros::NodeHandle n;

	if(config.useCameraInfo)
	{
		ROS_INFO_STREAM("Reading CameraInfo from topic");
		n.getParam("/lidar_camera_calibration/camera_info_topic", CAMERA_INFO_TOPIC);
		n.getParam("/lidar_camera_calibration/velodyne_topic", VELODYNE_TOPIC);

		message_filters::Subscriber<sensor_msgs::CameraInfo> info_sub(n, CAMERA_INFO_TOPIC, 1);
		message_filters::Subscriber<sensor_msgs::PointCloud2> cloud_sub(n, VELODYNE_TOPIC, 1);
		message_filters::Subscriber<lidar_camera_calibration::marker_6dof> rt_sub(n, "lidar_camera_calibration_rt", 1);

		std::cout << "done1\n";

		typedef sync_policies::ApproximateTime<sensor_msgs::CameraInfo, sensor_msgs::PointCloud2, lidar_camera_calibration::marker_6dof> MySyncPolicy;
		Synchronizer<MySyncPolicy> sync(MySyncPolicy(10), info_sub, cloud_sub, rt_sub);
		sync.registerCallback(boost::bind(&callback, _1, _2, _3));

		ros::spin();
	}
	else
	{
		ROS_INFO_STREAM("Reading CameraInfo from configuration file");
  		n.getParam("/lidar_camera_calibration/velodyne_topic", VELODYNE_TOPIC);

		message_filters::Subscriber<sensor_msgs::PointCloud2> cloud_sub(n, VELODYNE_TOPIC, 1);
		message_filters::Subscriber<lidar_camera_calibration::marker_6dof> rt_sub(n, "lidar_camera_calibration_rt", 1);

		typedef sync_policies::ApproximateTime<sensor_msgs::PointCloud2, lidar_camera_calibration::marker_6dof> MySyncPolicy;
		Synchronizer<MySyncPolicy> sync(MySyncPolicy(10), cloud_sub, rt_sub);
		sync.registerCallback(boost::bind(&callback_noCam, _1, _2));

		ros::spin();
	}
	
	return EXIT_SUCCESS;
}